# wastewater-management-optimization

## Description
We deal with the (waste)water management problem. We have a network of different types of components (inlet streams, intermedatiate nodes, outlet streams) and the respective components (wastewater streams, fresh water streams, treatment processes, applications, discharge points etc.). We aim at finding the optimal values of flowrates within the network in order to optimize a user-specified objective on a user specified component (node, edge or over the whole network). Currently, possible objectives are flows, cost and energy. 


This is a description of the attatched script. 

1) We execute wwmp_json_creation.py, which creates an input json file. If any attribute needs to be omitted, this is possible from the wwmp_data_creation.py.

2) We run wwmp_offline.py, which is the main function for the optimization algorithm. The input data are preprocessed in order to be transformed on the necessary data structures. Then, the wwmp_milp.py is called, which creates the optimization model and solves the network (currently, with the employment of the gurobi solver). Finally, wwmp_json_creation.py creates the final output in .json format. 

## Authors
People who have contributed to this project are Stavros Vatikiotis, Ioannis Avgerinos and Dr. George Zois.

## Acknowledgement
This work is supported by funds provided by the European Commission in the Horizon 2020 research and innovation programme AquaSPICE (Grant No. 958396).
<https://aquaspice.eu/>

## License
wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.

wastewater-management-optimization is licensed under a
Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.
