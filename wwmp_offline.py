#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import json
import datetime
import wwmp_validation
import wwmp_preprocessing
import time
import wwmp_milp
import wwmp_json_output

if __name__ == "__main__":
    print(datetime.datetime.now(), " - Algorithm Start.")

    f = open("wwmp_sample_input.json", )

    input = json.load(f)
    uuid = 100
    input_data = input['data']
    print(input_data)
    print(datetime.datetime.now(), " - Data Received Successfully.")

    # data validation
    problems = {}
    problems['data'] = {}
    try:
        data_ok, problems['data']['message'] = wwmp_validation.data_validation(input_data)
        if data_ok:
            # data preprocessing
            config_data, id_name, pollutants, edges, attributes, lower, upper, red_r_var, red_r_fixed, quality_parameters, i_minus, i_plus, dummy_node_flag, specialConstraints = wwmp_preprocessing.data_preprocessing(
                input_data)

            # optimizer milp
            flow_rates, concentration, edges_set, solver_status = wwmp_milp.milp(config_data, id_name, pollutants,
                                                                                 edges, attributes, lower, upper,
                                                                                 red_r_var, red_r_fixed,
                                                                                 quality_parameters, i_minus, i_plus,
                                                                                 specialConstraints)

            # output
            if solver_status != "infeasible" and solver_status != "infeasibleOrUnbounded":
                output = wwmp_json_output.json_output(config_data, id_name, pollutants, edges, attributes, red_r_var,
                                                      red_r_fixed, quality_parameters, flow_rates, concentration,
                                                      dummy_node_flag, i_minus, i_plus)

            else:
                output = {'data': {"status": "infeasible"}}
            output['uuid'] = uuid
            output['produced_at'] = int(time.time() * 1000)
            filename = "wwmp_out.json"
            with open(filename, 'w') as outfile:
                json.dump(output, outfile)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

        else:
            print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
            problems['uuid'] = uuid
            problems['produced_at'] = int(time.time() * 1000)
            filename = "wwmp_invalid_input.json"
            with open(filename, 'w') as outfile:
                json.dump(problems, outfile)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

    except Exception as e:
        print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
        print(str(e))
        error_message = {"message": str(e)}
        error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
        filename = "wwmp_exception.json"
        with open(filename, 'w') as outfile:
            json.dump(error_response, outfile)
        print(f"{datetime.datetime.now()}  - Algorithm End.")

