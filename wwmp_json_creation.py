#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import random
import wwmp_data_creation
import time
import json

if __name__ == "__main__":
    final = {}
    number_of_instances = 1
    for instance in range(number_of_instances):
        instance_number = time.time() + instance
        final[instance_number] = {}
        final[instance_number]['FWS'], final[instance_number]['Treatment'], final[instance_number]['WWS'], \
            final[instance_number]['Discharge'], final[instance_number]['App'] = {}, {}, {}, {}, {}

    fresh_water, tanks, treatment, wastewater, applications, discharges = 2, 1, 4, 1, 1, 2
    id_counter = 0
    for i in range(fresh_water):
        final[instance_number]['FWS'][f"FW_{i}"] = wwmp_data_creation.data_creation_fw()
        final[instance_number]['FWS'][f"FW_{i}"]["ID"] = id_counter
        id_counter += 1

    for i in range(tanks):
        final[instance_number]['Treatment'][f"T_{i}"] = wwmp_data_creation.data_creation_tank()
        final[instance_number]['Treatment'][f"T_{i}"]["ID"] = id_counter
        id_counter += 1

    for i in range(treatment):
        final[instance_number]['Treatment'][f"Tr_{i}"] = wwmp_data_creation.data_creation_treatment()
        final[instance_number]['Treatment'][f"Tr_{i}"]["ID"] = id_counter
        id_counter += 1

    for i in range(wastewater):
        final[instance_number]['WWS'][f"WW_{i}"] = wwmp_data_creation.data_creation_ww()
        final[instance_number]['WWS'][f"WW_{i}"]["ID"] = id_counter
        id_counter += 1

    for i in range(discharges):
        final[instance_number]['Discharge'][f"D_{i}"] = wwmp_data_creation.data_creation_discharge()
        final[instance_number]['Discharge'][f"D_{i}"]["ID"] = id_counter
        id_counter += 1

    for i in range(applications):
        final[instance_number]['App'][f"App_{i}"] = wwmp_data_creation.data_creation_app()
        final[instance_number]['App'][f"App_{i}"]["ID"] = id_counter
        id_counter += 1

    # Edges

    final[instance_number]['Edges'] = wwmp_data_creation.data_creation_edges()  # Edges: Connections
    final_simple = final.copy()

    # create the objective

    if random.uniform(0, 1) < 0.99:
        # multi-objective
        objective = "flow;energy"
        sense = 'min'
        a = min(random.randint(0, 10) / 10, 0.1)
        weights = str(a) + ';' + str(1 - a)
    else:
        # single objective
        objective = random.choice(["flow", "energy"])
        if objective == "ww":
            sense = "max"
        else:
            sense = "min"
        weights = "1"

    config = {'obj': objective, 'weights': weights, 'sense': sense}
    config["metrics"] = "ww; fw; energy; cost; discharge"
    config["where"] = "0-1+1-2;total"
    final_official = {'data': {}}
    final_official["data"] = {'config': config, 'network': final.pop(instance_number)}

with open("wwmp_sample_input.json", "w") as outfile:
    json.dump(final_official, outfile)
