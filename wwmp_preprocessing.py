#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import copy
import random
import numpy as np
from datetime import datetime

'''
config_data: dict --> keys: [obj, sense, metrics, where]
id_name: row: len(ID), columns: [ID, Category, Name, list(flow from components), list(flow to components)]
lower: row: len(ID), columns: value per pollutant
upper: row: len(ID), columns: value per pollutant
pollutants: list of pollutants
attributes: row: len(ID), columns[water_flow, demand, capacity, s_fixed, s_var, cost_fixed, cost_var, energy_fixed, energy_var]
red_r_var: row: len(ID), columns: value per pollutant
red_r_fixed: row: len(ID), columns: value per pollutant
quality parameters: row: len(ID), columns: pollutants
'''


def data_preprocessing(refined_data):
    print(datetime.now(), " - Start of Data Preprocessing.")
    dummy_node_flag = False  # check if dummy nodes have been created
    specialConstraints = {}
    if 'specialConstraints' in refined_data.keys():
        specialConstraints = {key: {} for key in refined_data['specialConstraints'].keys()}
        if 'conflicts' in specialConstraints.keys():
            for key in refined_data['specialConstraints']['conflicts'].keys():
                specialConstraints['conflicts'][key] = refined_data['specialConstraints']['conflicts'][key].split(";")
        if 'bindings' in specialConstraints.keys():
            for key in refined_data['specialConstraints']['bindings'].keys():
                specialConstraints['bindings'][key] = refined_data['specialConstraints']['bindings'][key].split(";")
    config_data = refined_data['config']
    if config_data['where'] != 'total':
        config_data['where'] = config_data['where'].split(';')
    network = refined_data['network']
    # id_mapping = {}
    # component_dict = {}  # dictionary with components
    pollutants = []  # quality parameters list
    id_name = {}
    quality_parameters = {}
    lower = {}
    upper = {}
    red_r_fixed = {}
    red_r_var = {}
    attributes = {}

    # Gather list of quality parameters (pollutants: list)

    for key, item in network.items():
        new_dict = network[key]
        for key2, item2 in new_dict.items():
            if key != "Edges":
                id = item2['ID']
                attributes[id] = {}
                id_name[id] = {'Cat': key, 'Name': key2}
                if "Req" in list(item2.keys()):
                    if 'Lower' in item2['Req'].keys():
                        for k in list(item2["Req"]["Lower"].keys()):
                            pollutants.append(k)
                    if 'Upper' in item2['Req'].keys():
                        for k in list(item2["Req"]["Upper"].keys()):
                            pollutants.append(k)
                if "QP" in list(item2.keys()):
                    quality_parameters[id] = {}
                    for k in list(item2["QP"].keys()):
                        quality_parameters[id][k] = item2["QP"][k]
                        pollutants.append(k)
                if "S_fixed" in list(item2.keys()):
                    attributes[id]['S_fixed'] = item2["S_fixed"]
                elif "S_var" in list(item2.keys()):
                    attributes[id]['S_var'] = item2["S_var"]
                else:
                    attributes[id]['S_var'] = 1
                if "capacity" in list(item2.keys()):
                    attributes[id]['capacity'] = item2["capacity"]
                if "demand" in list(item2.keys()):
                    attributes[id]['demand'] = item2["demand"]
                if "Water_flow" in list(item2.keys()):
                    attributes[id]['Water_flow'] = item2["Water_flow"]
                if "cost_fixed" in list(item2.keys()):
                    attributes[id]['cost_fixed'] = item2["cost_fixed"]
                if "cost_var" in list(item2.keys()):
                    attributes[id]['cost_var'] = item2["cost_var"]
                if "energy_fixed" in list(item2.keys()):
                    attributes[id]['energy_fixed'] = item2["energy_fixed"]
                if "energy_var" in list(item2.keys()):
                    attributes[id]['energy_var'] = item2["energy_var"]
                if 'special' in list(item2.keys()):
                    attributes[id]['special'] = 1
    pollutants = list(set(pollutants))
    for key, item in network.items():
        new_dict = network[key]
        for key2, item2 in new_dict.items():
            if key != "Edges":
                id = item2['ID']
                if "QP" in list(item2.keys()):
                    quality_parameters[id] = {}
                    for p in pollutants:
                        if p in list(item2["QP"].keys()):
                            quality_parameters[id][p] = item2["QP"][p]
                        else:
                            quality_parameters[id][p] = 0
                if "Req" in list(item2.keys()):
                    if 'Lower' in item2['Req'].keys():
                        lower[id] = {}
                        for k in list(item2["Req"]["Lower"].keys()):
                            lower[id][k] = item2['Req']['Lower'][k]
                    if 'Upper' in item2['Req'].keys():
                        upper[id] = {}
                        for k in list(item2["Req"]["Upper"].keys()):
                            upper[id][k] = item2['Req']['Upper'][k]
                red_r_var[id] = {}
                if "R_fixed" in item2.keys():
                    red_r_fixed[id] = {}
                if "R_fixed" in item2.keys():
                    for p in pollutants:
                        if p in item2["R_fixed"]:
                            red_r_fixed[id][p] = item2["R_fixed"][p]
                        else:
                            red_r_var[id][p] = 1
                if "R_var" in item2.keys():
                    for p in pollutants:
                        if p in item2["R_var"].keys():
                            red_r_var[id][p] = item2["R_var"][p]
                        else:
                            if "R_fixed" in item2.keys():
                                if p not in item2["R_fixed"]:
                                    red_r_var[id][p] = 1
                            else:
                                red_r_var[id][p] = 1

                if "R_var" not in item2.keys() and "R_fixed" not in item2.keys():
                    red_r_var[id] = {}
                    for p in pollutants:
                        red_r_var[id][p] = 1

    # get edges and capacities
    edges = {}
    for i in network['Edges'].keys():
        start_node, end_node = int(i.split('-')[0]), int(i.split('-')[1])
        edges[(start_node, end_node)] = {}
        # print(network['Edges'][i])
        if "capacity" in network['Edges'][i].keys():
            edges[(start_node, end_node)]["capacity"] = network['Edges'][i]['capacity']
        else:
            edges[(start_node, end_node)]["capacity"] = "No"
        if "cost_var" in network['Edges'][i].keys():
            edges[(start_node, end_node)]["cost"] = network['Edges'][i]['cost_var'] if network['Edges'][i][
                                                                                           'cost_var'] != "No" else 0
    if 'specialConstraints' in refined_data.keys():
        if 'conflicts' in specialConstraints.keys():
            for cons in specialConstraints['conflicts'].keys():
                new_cons = []
                for e in specialConstraints['conflicts'][cons]:
                    first, second = e.split("-")[0], e.split("-")[1]
                    new_cons.append((int(first), int(second)))
                specialConstraints['conflicts'][cons] = new_cons
        if 'bindings' in specialConstraints.keys():
            for cons in specialConstraints['bindings'].keys():
                new_cons = []
                for e in specialConstraints['bindings'][cons]:
                    first, second = e.split("-")[0], e.split("-")[1]
                    new_cons.append((int(first), int(second)))
                specialConstraints['bindings'][cons] = new_cons
    i_plus = {i: [] for i in id_name.keys()}
    i_minus = {i: [] for i in id_name.keys()}
    for e in edges.keys():
        i_plus[e[0]].append(e)
        i_minus[e[1]].append(e)
    counter = -1
    Ready = False
    ids = list(id_name.keys())
    while not Ready:
        Ready = True
        for i in ids:
            if len(i_minus[i]) > 2:
                Ready = False
                dummy_node_flag = True
                id_name[counter] = {'Cat': 'Treatment', 'Name': 'DUMMY' + "-" + str(id_name[i]['Name'])}
                attributes[counter] = {'S_var': 1}
                red_r_var[counter] = {p: 1 for p in pollutants}
                # two nodes to go to the dummy node
                first_node, second_node = i_minus[i][0][0], i_minus[i][1][0]
                i_minus[i] = i_minus[i][2:]
                i_minus[i].append((counter, i))
                i_minus[counter] = [(first_node, counter), (second_node, counter)]
                i_plus[counter] = [(counter, i)]
                edges[(counter, i)] = {'cost': "No", "capacity": "No"}
                for node_to_upd in [first_node, second_node]:
                    for k in i_plus[node_to_upd]:
                        if k == (node_to_upd, i):
                            i_plus[node_to_upd].append((node_to_upd, counter))
                            i_plus[node_to_upd].remove((node_to_upd, i))
                            edges[(node_to_upd, counter)] = edges[(node_to_upd, i)]
                            if str(node_to_upd) + '-' + str(i) in config_data['where']:
                                ind_obj = config_data['where'].index(str(node_to_upd) + '-' + str(i))
                                config_data['where'][ind_obj] = str(node_to_upd) + '-' + str(counter)
                            edges.pop((node_to_upd, i))
                        if 'specialConstraints' in refined_data.keys():
                            if 'conflicts' in specialConstraints:
                                for cons in specialConstraints['conflicts']:
                                    if k in specialConstraints['conflicts'][cons]:
                                        ind_k = specialConstraints['conflicts'][cons].index(k)
                                        specialConstraints['conflicts'][cons][ind_k] = (node_to_upd, counter)
                            if 'bindings' in specialConstraints:
                                for cons in specialConstraints['bindings']:
                                    if k in specialConstraints['bindings'][cons]:
                                        ind_k = specialConstraints['bindings'][cons].index(k)
                                        specialConstraints['bindings'][cons][ind_k] = (node_to_upd, counter)
                counter -= 1
    print(datetime.now(), " - End of Data Preprocessing.")
    return config_data, id_name, pollutants, edges, attributes, lower, upper, red_r_var, red_r_fixed, quality_parameters, i_minus, i_plus, dummy_node_flag, specialConstraints
