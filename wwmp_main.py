#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import json
import datetime
import wwmp_validation
import wwmp_preprocessing
import wwmp_milp
import wwmp_json_output
import time
import pika
import sys

host = sys.argv[1]
port = sys.argv[2]
username = sys.argv[3]
password = sys.argv[4]

credentials = pika.PlainCredentials(username, password)
params = pika.ConnectionParameters(host, port, '/', credentials, heartbeat=1860, blocked_connection_timeout=930)
connection = pika.BlockingConnection(params)
channel = connection.channel()


def callback(ch, method, properties, body):
    print(datetime.datetime.now(), " - Algorithm Start.")
    # print(" [x] Received %r" % body.decode())

    input = json.loads(body)
    print(datetime.datetime.now(), " - Data Received Successfully.")
    uuid = input['uuid']
    print(f"Process UUID: {input['uuid']}")
    print(input['data'])
    input_data = input['data']

    # data validation
    problems = {}
    problems['data'] = {}
    try:
        data_ok, problems['data']['message'] = wwmp_validation.data_validation(input_data)

        if data_ok:
            # data preprocessing
            config_data, id_name, pollutants, edges, attributes, lower, upper, red_r_var, red_r_fixed, quality_parameters, i_minus, i_plus, dummy_node_flag, specialConstraints = wwmp_preprocessing.data_preprocessing(
                input_data)

            # optimizer milp

            flow_rates, concentration, edges_set, solver_status = wwmp_milp.milp(config_data, id_name, pollutants,
                                                                                 edges, attributes, lower,
                                                                                 upper, red_r_var, red_r_fixed,
                                                                                 quality_parameters, i_minus, i_plus,
                                                                                 specialConstraints)

            # output
            if solver_status != "infeasible":
                output = wwmp_json_output.json_output(config_data, id_name, pollutants, edges, attributes, red_r_var,
                                                      red_r_fixed, quality_parameters, flow_rates, concentration,
                                                      dummy_node_flag, i_minus, i_plus)

            else:
                output = {'data': {"status": "infeasible"}}
            output['uuid'] = uuid
            output['produced_at'] = int(time.time() * 1000)
            output_json = json.dumps(output)
            print (output_json)
            print(f"{datetime.datetime.now()}  - Algorithm End.")
            channel.basic_publish(exchange='opt-result', routing_key='wwmp', body=output_json)


        else:
            print(f"{datetime.datetime.now()}  - Incorrect Input - data failed validation stage.")
            problems['uuid'] = uuid
            problems['produced_at'] = int(time.time() * 1000)
            output = json.dumps(problems)
            channel.basic_publish(exchange='opt-result', routing_key='wwmp', body=output)
            print(f"{datetime.datetime.now()}  - Algorithm End.")

    except Exception as e:
        print(f"{datetime.datetime.now()}  - Algorithm killed - an error occurred, check your data.")
        print(str(e))
        error_message = {"message": str(e)}
        error_response = {"uuid": uuid, "data": error_message, "produced_at": int(time.time() * 1000)}
        error_response = json.dumps(error_response)
        channel.basic_publish(exchange='opt-result', routing_key='wwmp', body=error_response)
        print(f"{datetime.datetime.now()}  - Algorithm End.")


channel.basic_consume(queue='wwmp_job', on_message_callback=callback, auto_ack=True)
channel.start_consuming()
