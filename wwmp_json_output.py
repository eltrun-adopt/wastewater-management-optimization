#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.

import random
import time
import json
import numpy as np
from datetime import datetime


def json_output(config_data, id_name, pollutants, edges, attributes, red_r_var, red_r_fixed, quality_parameters,
                flow_rates, concentration, dummy_node_flag, i_minus, i_plus):
    print(datetime.now(), " - Start of Output Creation.")
    metrics = {}
    if "energy" in config_data['metrics']:
        metrics['energy'] = 0
    if "cost" in config_data['metrics']:
        metrics['cost'] = 0
    if "ww" in config_data['metrics']:
        metrics['ww_used'] = 0
    if "fw" in config_data['metrics']:
        metrics['fw_intake'] = 0
    if "discharge" in config_data['metrics']:
        metrics['water_discharged'] = 0

    output = {}
    inlet, outlet = {}, {}
    flow = {}
    if dummy_node_flag:
        for i in id_name.keys():
            if i < 0:
                start_nodes = []
                for j in i_minus[i]:
                    start_nodes.append(j[0])
                end_node = i_plus[i][0][1]
                end_node_minus = [j for j in i_minus[end_node] if i not in j]
                for k in start_nodes:
                    flow_rates[k, end_node] = flow_rates[k, i]
                    flow_rates.pop((k, i))
                    edges[(k, end_node)] = edges[(k, i)]
                    edges.pop((k, i))
                    for ends in range(len(i_plus[k])):
                        if i_plus[k][ends][1] == i:
                            i_plus[k][ends] = (k, end_node)
                            end_node_minus.append((k, end_node))
                i_minus[end_node] = end_node_minus
                flow_rates.pop((i, end_node))
    id_name = {key: item for key, item in id_name.items() if key >= 0}
    IDs = id_name.keys()
    types = []
    for i in IDs:
        types.append(id_name[i]["Cat"])
    types = list(set(types))
    components = {i: {} for i in types}
    for i in id_name.keys():
        inlet[i], outlet[i] = {}, {}
        for j in components.keys():
            if id_name[i]["Cat"] == j:
                components[j][i] = {"Name": id_name[i]["Name"]}
                if "energy" in config_data['metrics']:
                    energy_i_fixed, energy_i_var = 0, 0
                    for edge_i in flow_rates:
                        if edge_i[0] == i or edge_i[1] == i:
                            if "energy_fixed" in attributes[i]:
                                if attributes[i]["energy_fixed"] != "No":  # calculate fixed energy
                                    energy_i_fixed = attributes[i]["energy_fixed"]
                            if "energy_var" in attributes[i]:
                                if attributes[i]["energy_var"] != "No":  # calculate var energy
                                    energy_i_var = attributes[i]["energy_var"] * flow_rates[edge_i]
                        components[j][i]["energy"] = energy_i_fixed + energy_i_var
                        metrics['energy'] += components[j][i]["energy"]
                if "cost" in config_data['metrics']:
                    cost_i_fixed, cost_i_var = 0, 0
                    for edge_i in flow_rates:
                        if edge_i[0] == i or edge_i[1] == i:
                            if "cost_fixed" in attributes[i]:
                                if attributes[i]["cost_fixed"] != "No":  # calculate fixed cost
                                    cost_i_fixed = attributes[i]["cost_fixed"]
                            if "cost_var" in attributes[i]:
                                if attributes[i]["cost_var"] != "No":  # calculate var cost
                                    cost_i_var = attributes[i]["cost_var"]
                    components[j][i]["cost"] = cost_i_fixed + cost_i_var
                    metrics['cost'] += components[j][i]["cost"]
                if "fw" in config_data['metrics'] and j == "FWS":
                    for edge_i in flow_rates:
                        if edge_i[0] == i:
                            metrics['fw_intake'] += flow_rates[edge_i]
                if "ww" in config_data['metrics'] and j == "WWS":
                    for edge_i in flow_rates:
                        if edge_i[0] == i:
                            metrics['ww_used'] += flow_rates[edge_i]
                if "discharge" in config_data['metrics'] and j == "Discharge":
                    for edge_i in flow_rates:
                        if edge_i[1] == i:
                            metrics['water_discharged'] += flow_rates[edge_i]
    calc_in = {i: False for i in id_name.keys()}
    calc_out = {i: False for i in id_name.keys()}
    for p in pollutants:
        for k in id_name.keys():
            id = k
            inlet[id][p], outlet[id][p] = 0, 0
            if len(i_minus[k]) == 0:
                inlet[id][p] = "No"
                outlet[id][p] = quality_parameters[k][p]
                calc_in[id] = True
                calc_out[id] = True
            elif len(i_plus[k]) == 0:
                outlet[id][p] = "No"
                calc_out[id] = True

    done = False
    start_time = time.time()
    while not done and time.time() - start_time < 30:
        done = True
        for k in id_name.keys():
            if calc_in[k] == False:
                prev = []
                for j in i_minus[k]:
                    prev.append(j[0])
                prev_bool = [calc_out[i] for i in prev]
                if all(prev_bool) and sum(flow_rates[b, k] for b in prev) != 0:
                    for p in pollutants:
                        # print (prev, id, sum(flow_rates[b, id] for b in prev))
                        inlet[k][p] = round(
                            sum(outlet[b][p] * flow_rates[b, k] for b in prev) / (
                                sum(flow_rates[b, k] for b in prev)), 3)
                        if outlet[k][p] != "No":
                            if k in red_r_fixed.keys():
                                if p in red_r_fixed[k].keys():
                                    outlet[k][p] = round(red_r_fixed[k][p] * inlet[k][p], 3)
                            if k in red_r_var.keys():
                                if p in red_r_var[k].keys():
                                    outlet[k][p] = round(red_r_var[k][p] * inlet[k][p], 3)
                        calc_in[k], calc_out[k] = True, True
                elif sum(flow_rates[b, k] for b in prev) == 0:
                    for p in pollutants:
                        inlet[k][p], outlet[k][p] = 0, 0
                    calc_in[k], calc_out[k] = True, True
        for k in id_name.keys():
            if calc_in[k] == False or calc_out[k] == False:
                done = False

    if done:
        for i in flow_rates.keys():
            node_1, node_2 = i[0], i[1]
            flow[str(node_1) + "-" + str(node_2)] = {"Flow_rate": round(flow_rates[node_1, node_2], 2)}
            if "cost" in config_data['metrics']:
                if "cost" in edges[node_1, node_2].keys():
                    flow[str(node_1) + "-" + str(node_2)]["cost"] = round(
                        flow_rates[node_1, node_2] * edges[node_1, node_2]['cost'], 2)
                    metrics['cost'] += round(flow[str(node_1) + "-" + str(node_2)]["cost"],2)
                else:
                    flow[str(node_1) + "-" + str(node_2)]["cost"] = 0

        output['data'] = {'status': 'feasible', 'Components': components, "Edges": flow, "Inlet": inlet,
                          "Outlet": outlet, "Metrics": metrics}
    else:
        output['data'] = {'message': 'Algorithm killed - probable cause: missing data.'}
    print(datetime.now(), " - End of Output Creation.")
    return output
