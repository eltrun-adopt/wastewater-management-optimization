#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import math
import numpy as np
import pandas as pd
import random


def data_creation_fw():
    fw = {}
    fw['QP'] = {}
    fw['QP']['p1'] = round(random.uniform(6, 8), 3)
    fw['QP']['p2'] = round(random.uniform(10, 500), 3)
    fw['QP']['p3'] = round(random.uniform(10, 35), 3)
    fw['QP']['p4'] = round(random.uniform(40, 100), 3)
    fw['QP']['p5'] = round(random.uniform(30, 100), 3)
    # fw['capacity'] = round(random.uniform(300, 500), 2)
    fw['cost_fixed'] = round(random.uniform(300, 1000), 2)
    fw['cost_var'] = round(random.uniform(20, 80), 2)
    return fw


def data_creation_treatment():
    tb = {}

    tb['S_var'] = round(random.uniform(0.85, 0.95), 3)

    # tb['capacity'] = random.randint(300, 650)
    tb['cost_fixed'] = random.randint(3000, 6500)
    tb['cost_var'] = random.randint(30, 100)
    tb['energy_var'] = random.randint(30, 65)
    tb['energy_fixed'] = random.randint(300, 650)
    tb['R_var'] = {}
    tb['R_fixed'] = {}
    tb['R_fixed']['p1'] = 8
    # tb['R_fixed']['p2'] = 10
    tb['R_var']['p3'] = round(random.uniform(0.1, 0.44), 3)
    tb['R_var']['p4'] = round(random.uniform(0.8, 0.9), 3)
    tb['R_var']['p5'] = round(random.uniform(0.9, 0.9), 3)
    return tb


def data_creation_discharge():
    d_ab = {}
    d_ab['Req'] = {}
    d_ab['Req']['Lower'], d_ab['Req']['Upper'] = {}, {}
    d_ab['Req']['Lower']['p1'], d_ab['Req']['Upper']['p1'] = 0, 1400
    d_ab['Req']['Lower']['p2'], d_ab['Req']['Upper']['p2'] = 0, 2500
    d_ab['Req']['Lower']['p3'], d_ab['Req']['Upper']['p3'] = 0, 1000
    d_ab['Req']['Lower']['p4'], d_ab['Req']['Upper']['p4'] = 0, 2000
    d_ab['Req']['Lower']['p5'], d_ab['Req']['Upper']['p5'] = 0, 1000
    d_ab['cost_fixed'] = random.randint(3000, 6500)
    d_ab['cost_var'] = random.randint(30, 100)

    return d_ab


def data_creation_ww():
    ww = {}
    ww['Water_flow'] = round(random.uniform(30, 100), 3)
    ww['QP'] = {}
    ww['QP']['p1'] = round(random.uniform(9, 11), 3)
    ww['QP']['p2'] = round(random.uniform(30, 200), 3)
    ww['QP']['p3'] = round(random.uniform(30, 150), 3)
    ww['QP']['p4'] = round(random.uniform(20, 150), 3)
    ww['QP']['p5'] = round(random.uniform(25, 100), 3)
    return ww


def data_creation_tank():
    wwr = {}
    # wwr['capacity'] = random.randint(1000, 2500)
    wwr['S_var'] = 1
    wwr['R_var'] = {}
    wwr['R_var']['p1'] = 1
    wwr['R_var']['p2'] = 1
    wwr['R_var']['p3'] = 1
    wwr['R_var']['p4'] = 1
    wwr['R_var']['p5'] = 1
    return wwr


def data_creation_app():
    app = {}
    app['demand'] = random.randint(200, 300)
    app['Req'] = {}
    app['Req']['Lower'], app['Req']['Upper'] = {}, {}
    app['Req']['Lower']['p1'], app['Req']['Upper']['p1'] = 0, 1500
    app['Req']['Lower']['p2'], app['Req']['Upper']['p2'] = 0, 1500
    app['Req']['Lower']['p3'], app['Req']['Upper']['p3'] = 0, 1500
    app['Req']['Lower']['p4'], app['Req']['Upper']['p4'] = 0, 1000
    app['Req']['Lower']['p5'], app['Req']['Upper']['p5'] = 0, 3000
    return app


def data_creation_edges():
    ed = {}
    ed['0-1'] = {'cost_var': 40}
    ed['1-2'] = {'cost_var': "No"}
    ed['2-3'] = {'cost_var': 50}
    ed['2-4'] = {'cost_var': 30}
    ed['2-5'] = {'cost_var': "No"}
    ed['7-5'] = {'cost_var': 10}
    ed['5-6'] = {'cost_var': 5}
    ed['6-9'] = {'cost_var': "No"}
    ed['6-8'] = {'cost_var': 5}
    ed['4-10'] = {'cost_var': 40}
    ed['4-9'] = {'cost_var': "No"}
    ed['3-10'] = {'cost_var': 5}

    return ed
