#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import pandas as pd
from datetime import datetime


def data_validation(data_to_check):
    print(datetime.now(), " - Start of Data Validation.")

    problems = {}
    problems['network'] = {}
    problems["config"] = {}

    if isinstance(data_to_check, dict):
        problems['fileFormat'] = "Correct file format."

        # check if all keys exist

        if 'config' in data_to_check.keys():
            config = data_to_check['config']
        else:
            problems["config"] = "Config key must be included - correct key - 'config'."

        if 'network' in data_to_check.keys():
            ids = []
            network = data_to_check['network']
            for cat, attr in network.items():
                for comp, item in network[cat].items():
                    if "ID" not in item and cat != "Edges":
                        problems['network']['comp'] = f"Missing ID on component {comp}."
                    else:
                        if "ID" in item and cat != "Edges":
                            if item["ID"] not in ids:
                                ids.append(item["ID"])
                            else:
                                problems['network']['comp'] = f"Duplicate ID on component {comp}."
                    if cat == "FWS" or cat == "WWS":
                        if "QP" not in item:
                            problems['network']['comp'] = f"Component {comp} should include field 'QP'."
                        else:
                            if not item['QP']:
                                problems['network']['comp'] = f"'QP' values for component {comp} must be provided."
        else:
            problems["network"] = "Network key must be included - correct key - 'network'."

        if len(problems['config']) > 0 or len(problems['network']) > 0:
            data_ok = False
        else:
            data_ok = True
    else:
        problems["fileFormat"] = "Invalid json format."
        data_ok = False
    print(datetime.now(), " - End of Data Validation.")

    return data_ok, problems


def check_dates(date1, date2):
    correct_format = False
    if type(date1) == str and type(date2) == str:
        date1 = date1.replace(",", "")
        date2 = date2.replace(",", "")
        try:
            date1 = datetime.strptime(date1, '%m/%d/%Y %H:%M:%S')
            date2 = datetime.strptime(date2, '%m/%d/%Y %H:%M:%S')
            if date1 > date2:
                correct_format = True
        except Exception as e:
            correct_format = False
    return correct_format


def positive_int(a):
    correct_format = False
    if isinstance(a, int) and a > 0:
        correct_format = True
    return correct_format


def positive_float(a):
    correct_format = False
    if isinstance(a, float) and a > 0:
        correct_format = True
    return correct_format
