#    wastewater-management-optimization (c) by the Athens University of Economics and Business, Greece.
#
#    wastewater-management-optimization is licensed under a
#    Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License.
#
#    You should have received a copy of the license along with this
#    work.  If not, see <http://creativecommons.org/licenses/by-nc-nd/3.0/>.


import random
import math
from pyomo.environ import *
from pyomo.environ import Binary, NonNegativeReals
import json
# import gurobipy
# import cplex
import itertools
# from cplex.exceptions import CplexSolverError
import logging
from datetime import datetime
import numpy as np
from pyomo.util.infeasible import log_infeasible_constraints
import logging


def milp(config_data, id_name, pollutants, edges, attributes, lower_limits, upper_limits, red_r_var, red_r_fixed,
         quality_parameters, i_minus, i_plus, specialConstraints):
    print(datetime.now(), " - Start of Optimization Algorithm.")
    logging.getLogger('pyomo.core').setLevel(logging.ERROR)
    edges_set = list(edges.keys())
    big_M = 50000000
    small_m = 10 ** -2
    max_discrete = 200
    IDs = list(id_name.keys())
    blending_sets = []
    for i in IDs:
        if len(i_minus[i]) == 2:
            blending_sets.append(i_minus[i][0])
            blending_sets.append(i_minus[i][1])
    milp = ConcreteModel()
    milp.x = Var(edges_set, within=NonNegativeReals, initialize=0.0)
    milp.Y = Var(edges_set, within=Binary, initialize=0.0)
    milp.c = Var(IDs, pollutants, within=NonNegativeReals, initialize=0.0)
    milp.z = Var(blending_sets, [k for k in range(max_discrete + 1)], within=Binary, initialize=0.0)

    def obj_rule(model):
        objective_total = 0
        obj_list = config_data['obj'].split(";")
        weights = config_data['weights'].split(";")
        objectives = {}
        for i in obj_list:
            w_pos = obj_list.index(i)
            objectives[i] = {'weight': weights[w_pos], 'where': config_data["where"][w_pos]}
        for obj in obj_list:
            objective_sum = 0
            if obj == "flow":
                if objectives[obj]['where'] != "total":
                    edge_to_opt = objectives[obj]['where'].split('+')
                    for k in edge_to_opt:
                        i = k.split("-")
                        node_1, node_2 = int(i[0]), int(i[1])
                        objective_sum += milp.x[(node_1, node_2)]
                else:
                    for k in edges_set:
                        i = k.split("-")
                        node_1, node_2 = int(i[0]), int(i[1])
                        objective_sum += milp.x[(node_1, node_2)]
            elif obj == "cost":
                if objectives[obj]['where'] != "total":
                    for e in edges_set:
                        node_1, node_2 = int(e[0]), int(e[1])
                        if 'cost_var' in attributes[e[1]].keys():
                            objective_sum += milp.x[(node_1, node_2)] * attributes[e[1]]['cost_var']
                        if 'cost_fixed' in attributes[e[1]].keys():
                            objective_sum += milp.Y[(node_1, node_2)] * attributes[e[1]]['cost_fixed']
                        if edges[e]['cost'] != "No":
                            objective_sum += milp.x[e] * edges[e]['cost']
                else:
                    for e in edges_set:
                        node_1, node_2 = int(e[0]), int(e[1])
                        if 'cost_var' in attributes[e[1]].keys():
                            objective_sum += milp.x[(node_1, node_2)] * attributes[e[1]]['cost_var']
                        if 'cost_fixed' in attributes[e[1]].keys():
                            objective_sum += milp.Y[(node_1, node_2)] * attributes[e[1]]['cost_fixed']
                        if edges[e]['cost'] != "No":
                            objective_sum += milp.x[e] * edges[e]['cost']

            elif obj == "energy":
                if objectives[obj]['where'] != "total":
                    edge_to_opt = config_data["where"]
                    for k in edge_to_opt:
                        i = k.split("-")
                        node_1, node_2 = int(i[0]), int(i[1])
                        if attributes[node_2][5] != "No":
                            objective_sum += milp.x[(node_1, node_2)] * float(attributes[node_2][7]) + float(
                                attributes[node_2, 8])
                else:
                    for e in edges_set:
                        node_1, node_2 = int(e[0]), int(e[1])
                        if 'energy_var' in attributes[node_2] != "No":
                            objective_sum += milp.x[(node_1, node_2)] * float(attributes[node_2]['energy_var'])
                        if 'energy_fixed' in attributes[node_2]!= "No":
                            objective_sum += + float(attributes[node_2]['energy_fixed'])
            objective_total += float(objectives[obj]['weight']) * objective_sum
        return objective_total

    if config_data['sense'] == "max":
        milp.obj = Objective(rule=obj_rule, sense=maximize)
    else:
        milp.obj = Objective(rule=obj_rule, sense=minimize)
    milp.constraints = ConstraintList()

    for e in edges_set:
        # constraints (5) - edge capacity
        if edges[e]["capacity"] != "No":
            milp.constraints.add(milp.Y[e] * edges[e]["capacity"] >= milp.x[e])
        else:
            milp.constraints.add(milp.Y[e] * big_M >= milp.x[e])
        # constraints (6) - edge capacity small_m
        milp.constraints.add(milp.Y[e] * small_m <= milp.x[e])

    for i in IDs:
        if len(i_minus[i]) == 0:
            # constraints (1) -- fixed incoming flow
            if 'Water_flow' in attributes[i].keys():
                milp.constraints.add(sum(milp.x[e] for e in i_plus[i]) == attributes[i]['Water_flow'])
            # constraints (11) -- fixed quality parameters
            for p in pollutants:
                if quality_parameters[i][p] != "No":
                    milp.constraints.add(milp.c[i, p] == quality_parameters[i][p])

        if len(i_plus[i]) > 0:
            # constraints (3) -- capacity on the exit
            if 'capacity' in attributes[i].keys():
                milp.constraints.add(sum(milp.x[e] for e in i_plus[i]) <= attributes[i]['capacity'])

        if len(i_minus[i]) == 1:
            for p in pollutants:
                # constraints (25 - 26) -- contamination reduction (variable)
                if i in red_r_var.keys():
                    if p in red_r_var[i].keys():
                        milp.constraints.add(milp.c[i_minus[i][0][0], p] * red_r_var[i][p] - milp.c[
                            i, p] <= big_M * (1 - milp.Y[i_minus[i][0][0], i]))
                        milp.constraints.add(
                            milp.c[i, p] - milp.c[i_minus[i][0][0], p] * red_r_var[i][
                                p] <= big_M * (1 - milp.Y[i_minus[i][0][0], i]))

                # constraints (33) -- lower limits for contamination reduction (fixed)
                if i in lower_limits.keys():
                    if p in lower_limits[i].keys():
                        if i in red_r_fixed.keys():
                            if p in red_r_fixed[i].keys():
                                milp.constraints.add(milp.c[i_minus[i][0][0], p] >= lower_limits[i][p])

                # constraints (34) -- upper limits for contamination reduction (fixed)
                if i in upper_limits.keys():
                    if p in upper_limits[i].keys():
                        if i in red_r_fixed.keys():
                            if p in red_r_fixed[i].keys():
                                milp.constraints.add(milp.c[i_minus[i][0][0], p] <= upper_limits[i][p])

        if len(i_minus[i]) == 2:
            for j in i_minus[i]:
                # constraints (19) -- sum(z_{ijk}) == 1
                milp.constraints.add(sum(milp.z[j[0], i, k] for k in range(max_discrete + 1)) == 1.0)

            # constraints (20) - k-1 equal to k

            for k in range(1, max_discrete):
                milp.constraints.add(milp.z[i_minus[i][0][0], i, k] == milp.z[i_minus[i][1][0], i, max_discrete - k])

            # constraints (21 - 22) -- water losses on blending points

            for k in range(1, max_discrete):
                milp.constraints.add((max_discrete - k) * milp.x[i_minus[i][0]] - k * milp.x[i_minus[i][1]] <= big_M * (
                        1 - milp.z[i_minus[i][0], k]))

                milp.constraints.add(k * milp.x[i_minus[i][1]] - (max_discrete - k) * milp.x[i_minus[i][0]] <= big_M * (
                        1 - milp.z[i_minus[i][0], k]))

            # constraints (23 - 24) -- z_{ijk} for k == 0 and k == max_discrete
            milp.constraints.add(milp.z[i_minus[i][0][0], i, 0] + milp.Y[i_minus[i][0][0], i] == 1)
            milp.constraints.add(milp.z[i_minus[i][1][0], i, 0] + milp.Y[i_minus[i][1][0], i] == 1)

            milp.constraints.add(milp.z[i_minus[i][0][0], i, max_discrete] + milp.Y[i_minus[i][1][0], i] <= 1)
            milp.constraints.add(milp.z[i_minus[i][1][0], i, max_discrete] + milp.Y[i_minus[i][0][0], i] <= 1)

            for p in pollutants:
                # constraints (35 - 36) -- upper and lower limits for r_fixed
                if i in lower_limits.keys():
                    if p in lower_limits[i].keys():
                        if i in red_r_fixed.keys():
                            if p in red_r_fixed[i].keys():
                                # constraints (37 - 38) -- upper and lower limits for r_fixed
                                milp.constraints.add(
                                    lower_limits[i][p] <= milp.c[i_minus[i][0][0], p] + big_M * (
                                            2 - milp.z[i_minus[i][0], 0] - milp.Y[i_minus[i][0]]))

                                milp.constraints.add(
                                    lower_limits[i][p] <= milp.c[i_minus[i][0][0], p] + big_M * (
                                            1 - milp.z[i_minus[i][0], max_discrete]))
                                for k in range(1, max_discrete + 1):
                                    milp.constraints.add(lower_limits[i][p] <= ((k * milp.c[i_minus[i][0][0], p] + (
                                            max_discrete - k) * milp.c[i_minus[i][1][0], p]) / max_discrete) -
                                                         milp.c[i, p] + big_M * (1 - milp.z[i_minus[i][0], k]))
                if i in upper_limits.keys():
                    if p in upper_limits[i].keys():
                        if i in red_r_fixed.keys():
                            if p in red_r_fixed[i].keys():
                                # constraints (39 - 40) -- upper and lower limits for r_fixed
                                milp.constraints.add(
                                    upper_limits[i][p] >= milp.c[i_minus[i][0][0], p] - big_M * (
                                            2 - milp.z[i_minus[i][0], 0] - milp.Y[i_minus[i][0]]))

                                milp.constraints.add(
                                    upper_limits[i][p] >= milp.c[i_minus[i][0][0], p] - big_M * (
                                            1 - milp.z[i_minus[i][0], max_discrete]))
                                for k in range(1, max_discrete + 1):
                                    milp.constraints.add(upper_limits[i][p] >= ((k * milp.c[i_minus[i][0][0], p] + (
                                            max_discrete - k) * milp.c[i_minus[i][1][0], p]) / max_discrete) -
                                                         milp.c[i, p] - big_M * (1 - milp.z[i_minus[i][0], k]))

                # constraints (27 - 28) -- quality change on blending points
                if i in red_r_var.keys():
                    if p in red_r_var[i].keys():
                        for k in range(1, max_discrete):
                            milp.constraints.add(red_r_var[i][p] * ((k * milp.c[i_minus[i][0][0], p] + (
                                    max_discrete - k) * milp.c[i_minus[i][1][0], p]) / max_discrete) - milp.c[
                                                     i, p] <= big_M * (1 - milp.z[i_minus[i][0], k]))
                            milp.constraints.add(milp.c[i, p] - red_r_var[i][p] * ((k * milp.c[i_minus[i][0][0], p] + (
                                    max_discrete - k) * milp.c[i_minus[i][1][0], p]) / max_discrete) <= big_M * (
                                                         1 - milp.z[i_minus[i][0], k]))

                        # constraints (29 - 30) - cases with all/none from r
                        milp.constraints.add(milp.c[i_minus[i][0][0], p] * red_r_var[i][p] - milp.c[i, p] <= big_M * (
                                2 - milp.z[i_minus[i][1], 0] - milp.Y[i_minus[i][0]]))

                        milp.constraints.add(milp.c[i, p] - red_r_var[i][p] * milp.c[i_minus[i][0][0], p] <= big_M * (
                                2 - milp.z[i_minus[i][1], 0] - milp.Y[i_minus[i][0]]))

                        # constraints (31 - 32) - cases with all/none from i
                        milp.constraints.add(milp.c[i, p] - red_r_var[i][p] * milp.c[i_minus[i][1][0], p] <= big_M * (
                                1 - milp.z[i_minus[i][1], max_discrete]))

                        milp.constraints.add(milp.c[i_minus[i][1][0], p] * red_r_var[i][p] - milp.c[i, p] <= big_M * (
                                1 - milp.z[i_minus[i][1], max_discrete]))

        if len(i_minus[i]) > 0:
            # constraints (2) -- demand
            if 'demand' in attributes[i].keys():
                milp.constraints.add(sum(milp.x[e] for e in i_minus[i]) >= attributes[i]['demand'])
            # constraints (4) -- capacity on the entry
            if 'capacity' in attributes[i].keys():
                milp.constraints.add(sum(milp.x[e] for e in i_minus[i]) <= attributes[i]['capacity'])

            for p in pollutants:
                # constraints (13) -- lower limits for contamination reduction (variable)
                if i in lower_limits.keys():
                    if p in lower_limits[i].keys():
                        if i in red_r_var.keys():
                            if p in red_r_var[i].keys():
                                milp.constraints.add(milp.c[i, p] / red_r_var[i][p] >= lower_limits[i][p])

                # constraints (14) -- upper limits for contamination reduction (variable)
                if i in upper_limits.keys():
                    if p in upper_limits[i].keys():
                        if i in red_r_var.keys():
                            if p in red_r_var[i].keys():
                                milp.constraints.add(milp.c[i, p] / red_r_var[i][p] <= upper_limits[i][p])

            if len(i_plus[i]) > 0:

                # constraints (8 - 9 - 10) -- flow rate reduction fixed

                if 'S_fixed' in attributes[i]:
                    for e_in in i_minus[i]:
                        milp.constraints.add(
                            sum(milp.x[e] for e in i_plus[i]) <= attributes[i]['S_fixed'] + (1 - milp.Y[e_in]) * big_M)

                        milp.constraints.add(
                            sum(milp.x[e] for e in i_plus[i]) >= attributes[i]['S_fixed'] - (1 - milp.Y[e_in]) * big_M)

                    milp.constraints.add(sum(milp.x[e] for e in i_plus[i]) <= attributes[i]['S_fixed'] * sum(
                        milp.x[e] for e in i_minus[i]))

                # constraints (7) -- flow rate reduction variable
                if 'S_var' in attributes[i]:
                    milp.constraints.add(sum(milp.x[e] for e in i_plus[i]) == attributes[i]['S_var'] * sum(
                        milp.x[e] for e in i_minus[i]))

                # constraints (15 - 16) -- contamination reduction (fixed)
                if i in red_r_fixed.keys():
                    for p in pollutants:
                        if p in red_r_fixed[i].keys():
                            milp.constraints.add(
                                milp.c[i, p] - red_r_fixed[i][p] <= big_M * (1 - milp.Y[i_minus[i][0][0], i]))
                            milp.constraints.add(
                                red_r_fixed[i][p] - milp.c[i, p] <= big_M * (1 - milp.Y[i_minus[i][0][0], i]))

    # add conflicting and binding constraints

    if 'conflicts' in specialConstraints.keys():
        for cons in specialConstraints['conflicts']:
            milp.constraints.add(sum(milp.Y[e[0], e[1]] for e in specialConstraints['conflicts'][cons]) <= 1)

    if 'bindings' in specialConstraints.keys():
        for cons in specialConstraints['bindings']:
            milp.constraints.add(
                milp.Y[specialConstraints['bindings'][cons][0]] == milp.Y[specialConstraints['bindings'][cons][1]])
    opt = SolverFactory("gurobi", solver_io='python')
    #opt.options['IntegralityFocus'] = 1
    #opt.options['IntFeasTol'] = 1e-9
    #opt.options['FeasibilityTol'] = 1e-9
    opt.options['timelimit'] = 120
    opt.options['MIPGap'] = 0.01
    #milp.pprint()

    results_obj = opt.solve(milp, tee=True)
    data = results_obj.Problem._list
    lower_bound, upper_bound = data[0].lower_bound, data[0].upper_bound
    solver_status = results_obj.Solver()['Termination condition'].value
    flow_rates, concentration = {}, {k: {} for k in id_name.keys()}
    for v in milp.component_objects(Var, active=True):
        # print("Variable component object", v)
        # print("Type of component object: ", str(type(v))[1:-1])  # Stripping <> for nbconvert
        varobject = getattr(milp, str(v))
        # print("Type of object accessed via getattr: ", str(type(varobject))[1:-1])

        for index in varobject:
            # if varobject[index].value != 0:
            #    print("   ", index, varobject[index].value)
            if str(v) == "x":
                # print("   ", (ind_1, ind_2), varobject[index].value)
                # print("   ", (index[0], index[1]), varobject[index].value)
                flow_rates[index[0], index[1]] = round(varobject[index].value, 2)
                print(index, flow_rates[index[0], index[1]])

            elif str(v) == "c":
                # print("c", index, varobject[index].value)
                concentration[index[0]][index[1]] = round(varobject[index].value, 5)
            elif str(v) == "Y":
                if varobject[index].value > 0.9:
                    print("Y", index, varobject[index].value)
            elif str(v) == "z":
                if varobject[index].value > 0.9:
                    print("Z", index, varobject[index].value)
    print(datetime.now(), " - End of Optimization Algorithm.")
    return flow_rates, concentration, edges_set, solver_status
